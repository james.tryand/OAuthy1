﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace RESOURCEY1.Controllers
{
    public class Bob
    {
        public string Name { get; set; }
    }
    [EnableCors(origins: "http://localhost:50556", headers: "*", methods: "*")]
    public class DefaultController : ApiController
    {
        //public async Task<HttpResponseMessage> Get()
        //{
        //    return new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new ByteArrayContent()
        //    }
        //}
        [System.Web.Http.HttpGet]
        public List<Bob> Index()
        {
            return Get();
        }

        //[System.Web.Http.HttpGet]
        public List<Bob> Get(int id = 0)
        {
            return  new List<Bob>() { new Bob() { Name = "Rod" }, new Bob() { Name = "Jane" }, new Bob() { Name = "Freddy" } };
        }
    }
}
