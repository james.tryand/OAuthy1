﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CLIENTY1.Models;

namespace CLIENTY1.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RainbowPeeps()
        {
            var RainbowPeeps = new List<RainbowPeep> { new RainbowPeep() { Name = "Zippy" }, new RainbowPeep() { Name = "George" } };

            return View(RainbowPeeps);
        }
    }
}